CREATE TABLE lab11_ex07_team1(
  name VARCHAR(20),
  city CHAR(3),
  captain VARCHAR(50),
  points INT,
  PRIMARY KEY (name)
);

CREATE TABLE lab11_ex07_team2(
  name VARCHAR(20),
  city CHAR(3),
  captain VARCHAR(50),
  points INT,
  PRIMARY KEY (name)
);

CREATE TABLE lab11_ex07_team3(
  name VARCHAR(20),
  city CHAR(3),
  captain VARCHAR(50),
  points INT,
  PRIMARY KEY (name)
);

CREATE TABLE lab11_ex07_players(
  id INT,
  name VARCHAR(20),
  city CHAR(3),
  captain VARCHAR(50),
  points INT,
  PRIMARY KEY (id)
);

