-- Answers to exercise 2 questions
# A. What are the names of the students who attend COMP219?
SELECT
  fname,
  lname,
  dept,
  num
FROM unidb_students AS s INNER JOIN unidb_attend AS a ON s.id = a.id
WHERE a.dept = 'comp' AND a.num=219;
# B. What are the names of the student reps that are not from NZ?
SELECT fname,lname,country FROM unidb_students WHERE country NOT LIKE 'nz';
# C. Where are the offices for the lecturers of 219?
SELECT office,num
FROM unidb_lecturers AS l JOIN unidb_teach t ON l.staff_no = t.staff_no
WHERE num=219;
# D. What are the names of the students taught by Te Taka?
# lecture
SELECT l.staff_no,fname,dept,num
FROM unidb_lecturers l INNER JOIN unidb_teach t ON l.staff_no = t.staff_no
WHERE fname='Te Taka';
# student
SELECT fname,lname,a.dept,a.num
FROM unidb_students s INNER JOIN unidb_attend a ON s.id = a.id;

SELECT ss.fname,ss.lname,ss.dept,ss.num,ll.fname
FROM (SELECT l.staff_no,fname,dept,num
      FROM unidb_lecturers l INNER JOIN unidb_teach t ON l.staff_no = t.staff_no
      WHERE fname='Te Taka') AS ll
INNER JOIN (SELECT fname,lname,a.dept,a.num
            FROM unidb_students s INNER JOIN unidb_attend a ON s.id = a.id) AS ss
ON ll.dept=ss.dept AND ll.num=ss.num;
# E. List the students and their mentors
SELECT s1.fname,s1.lname,s1.mentor,s2.fname,s2.lname
FROM unidb_students s1 LEFT JOIN unidb_students s2 ON s1.mentor=s2.id;
# F. Name the lecturers whose office is in G-Block as well naming the students that are not from NZ
# lecture
SELECT l.staff_no,fname,dept,num,office
FROM unidb_lecturers l INNER JOIN unidb_teach t ON l.staff_no = t.staff_no
WHERE office LIKE 'G%';
# student
SELECT fname,lname,a.dept,a.num,country
FROM unidb_students s INNER JOIN unidb_attend a ON s.id = a.id
WHERE NOT country='nz';

SELECT ss.fname,ss.lname,ss.dept,ss.num,ll.fname,ll.office
FROM (SELECT l.staff_no,fname,dept,num,office
      FROM unidb_lecturers l INNER JOIN unidb_teach t ON l.staff_no = t.staff_no
      WHERE office LIKE 'G%') ll
INNER JOIN (SELECT fname,lname,a.dept,a.num,country
            FROM unidb_students s INNER JOIN unidb_attend a ON s.id = a.id
            WHERE NOT country='nz') ss
ON ll.dept=ss.dept AND ll.num=ss.num;
# G. List the course co-ordinator and student rep for COMP219
SELECT c.*,s.fname,s.lname
FROM unidb_courses c INNER JOIN unidb_students s ON c.rep_id = s.id;
SELECT cs.*,l.fname,l.lname
FROM unidb_lecturers l 
INNER JOIN (SELECT c.*,s.fname,s.lname
            FROM unidb_courses c INNER JOIN unidb_students s ON c.rep_id = s.id) cs
ON cs.coord_no=l.staff_no;
