-- Answers to exercise 1 questions
# A. Display the departments offering courses
SELECT dept FROM unidb_courses GROUP BY dept;
# B. Display the semesters being attended
SELECT DISTINCT semester FROM unidb_attend;
# C. Display the courses that are attended
SELECT num FROM unidb_attend GROUP BY num;
# D. List the student names and country, ordered by first name
SELECT fname,lname,country FROM unidb_students ORDER BY fname;
# E. List the student names and mentors, ordered by mentors
SELECT fname,lname,mentor FROM unidb_students ORDER BY mentor;
# F. List the lecturers, ordered by office
SELECT * FROM unidb_lecturers ORDER BY office;
# G. List the staff whose staff number is greater than 500
SELECT * FROM unidb_lecturers WHERE staff_no>500;
# H. List the students whose id is greater than 1668 and less than 1824
SELECT * FROM unidb_students WHERE id BETWEEN 1668 AND 1824;
# I. List the students from NZ, Australia and US
SELECT * FROM unidb_students WHERE country IN ('NZ','AU','US');
# J. List the lecturers in G Block
SELECT * FROM unidb_lecturers WHERE office LIKE 'G%';
# K. List the courses not from the Computer Science Department
SELECT * FROM unidb_courses WHERE dept NOT LIKE 'comp';
# L. List the students from France or Mexico
SELECT * FROM unidb_students WHERE country ='mx' OR country='fr';
